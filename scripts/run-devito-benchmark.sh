#! /usr/bin/bash
set -x

# machine name - xeon_gold_5120 or xeon_gold_6126 or arm_thunderx2
machine=xeon_gold_5120

# pinning tool
nc="numactl --cpunodebind=1 --membind=1"

# number of runs
nruns=3

# number of cores of the processor
if [[ $machine == arm_thunderx2 ]]; then
    cores=32
else
    if [[ $machine == xeon_gold_5120 ]]; then
        cores=14
    else
        cores=12
    fi
fi

# available compilers and platform
if [[ $machine == arm_thunderx2 ]]; then
    plat="DEVITO_PLATFORM=arm"
    compilers="gcc cce"
else
    plat="DEVITO_PLATFORM=skx"
    compilers="gcc"
fi

# for each compiler
for compiler in $compilers; do

    # for each backend
    for be in core; do

        # for each problem
        for prob in acoustic tti; do

            if [[ $prob == tti ]]; then
                bms="O2"
            else
                bms="O1 O2 O3"
            fi

            # for each devito optimization mode
            for bm in $bms; do

                # for each grid size
                for sz in 512; do

                    # for each space order
                    for so in `seq 4 4 16`; do

                        dir=results-devito-benchmark/$machine/$compiler/$be/$prob/$bm/grid$sz

                        logdir=$dir/so$so
                        mkdir -p $logdir

                        log=$logdir/run.log
                        rm -f $log
                        touch $log
                        date >> $log
                        uname -a >> $log
                        lscpu >> $log
                        icpc -v 2>&1 >> $log
                        uname -a >> $log
                        numactl -H >> $log
                        numastat -m >> $log
                        top -b -n 1 | head -30 >> $log

                        cmd="env $plat OMP_NUM_THREADS=$cores DEVITO_DEBUG_COMPILER=1 DEVITO_AUTOTUNING=aggressive DEVITO_BACKEND=$be DEVITO_OPENMP=1 DEVITO_ARCH=$compiler DEVITO_LOGGING=DEBUG $nc python3 benchmarks/user/benchmark.py bench -bm $bm -P $prob -so $so -to 2 -d $sz $sz $sz --tn 1000 -x $nruns -r $dir"
                        echo $cmd >> $log
                        $cmd |& tee -a $log
                        date >> $log

                    done
                done
            done
        done
    done
done
