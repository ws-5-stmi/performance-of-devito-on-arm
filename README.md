# Performance of Devito on ARM and Intel Processors #

This repository contains scripts and results related to performance experiments of Devito running on Marvell ThunderX2 (Arm), Intel Xeon Gold 5120 and Intel Xeon Gold 6126.

### What do you need to reproduce the experiments? ###

* The following packages are required: ``build-essential numactl git python3 python3-pip``
* Install Devito v3.4 - You can unpack ``devito.tar.gz``, access the folder and run ``install-devito-intel.sh`` or ``install-devito-arm.sh`` (this one for Arm on a PBS system)
* Updated version of Devito can be found in [Devito Project](https://www.devitoproject.org)

### Results of Processors Scalability ###

* The results are available in the ``results-scalability`` directory
* The script to reproduce this experiment is available in ``scripts/run-scalability.sh``. Note the settings for each architecture (Arm ThunderX2, Intel 5120 and Intel 6126)
* Run the script inside Devito directory or change the path for ``benchmark.py`` in the script.

### Results of Devito Performance ###

* The results are available in the ``results-devito-benchmark`` directory
* The script to reproduce this experiment is available in ``scripts/run-devito-benchmark.sh``.
* The file ``machines.txt`` contains a description of the architectures used for the experiments, and It is necessary to generate the plots. The script ``run-plot.sh`` can be used to recreate the plots.

### Run Stream ###

* [Stream](https://www.cs.virginia.edu/stream/) is a tool to measure the memory bandwidth. The TRIAD measure of Stream is one of descriptions in the ``machines.txt``.
* In the ``stream`` directory are available: some results, the source code of Stream and a Makefile to compile the code. 
