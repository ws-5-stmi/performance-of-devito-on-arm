#! /usr/bin/bash

# machine name - xeon_gold_5120 or xeon_gold_6126 or arm_thunderx2
machine=xeon_gold_5120

# pinning tool
nc="numactl --cpunodebind=1 --membind=1"

# number of runs
nruns=3

# number of cores of the processor and options (for OMP_NUM_THREADS)
if [[ $machine == arm_thunderx2 ]]; then
    cores=32
    omp_options="1 2 4 8 16 32 64 128"
else
    if [[ $machine == xeon_gold_5120 ]]; then
        cores=14
        omp_options="1 2 4 8 14 28"
    else
        cores=12
        omp_options="1 2 4 8 12 24"
    fi
fi

# available compilers and platform
if [[ $machine == arm_thunderx2 ]]; then
    plat="DEVITO_PLATFORM=arm"
    compilers="gcc cce"
else
    plat="DEVITO_PLATFORM=skx"
    compilers="gcc"
fi

# for each compiler
for compiler in $compilers; do

    # for each problem
    for problem in acoustic tti; do

        if [ $problem == tti ]; then
            bms="O2"
        else
            bms="O3"
        fi

        # for each devito optimization mode
        for bm in $bms; do

            # for each number of OpenMP threads
            for threads in $omp_options; do

                dir=results-scalability/$machine/$compiler/$problem/$bm/cores-$threads
                mkdir -p $dir
                log=$dir/run.log
                rm -f $log
                touch $log

                date >> $log
                uname -a >> $log
                lscpu >> $log
                numactl -H >> $log
                numastat -m >> $log
                top -b -n 1 | head -30 >> $log

                if [ $threads == 1 ]; then
                    enable_openmp=0
                else
                    enable_openmp=1
                fi

                cmd="env OMP_NUM_THREADS=$threads $platform DEVITO_LOGGING=DEBUG DEVITO_DEBUG_COMPILER=1 DEVITO_AUTOTUNING=aggressive DEVITO_BACKEND=core DEVITO_OPENMP=$enable_openmp DEVITO_ARCH=$compiler $nc python3 benchmarks/user/benchmark.py bench -bm $bm -P $problem -so 4 -to 2 -d 512 512 512 --tn 1000 -x $nruns -r $dir"

                echo $cmd >> $log
                $cmd |& tee -a $log
                date >> $log

            done
        done
    done
done
