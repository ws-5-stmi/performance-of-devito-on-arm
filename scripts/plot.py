import os
import sys
from subprocess import call
import re

#
# Expected directory tree:
# archname/compiler/backend/problem-name/bench-mode/grid-size/

if len(sys.argv) != 2:
    print("Usage: python plot.py absolute/path/to/root")
    sys.exit(0)

machines = {}
exec(open("machines.txt").read(), {}, machines)

subfolders = ['archname', 'compiler', 'backend', 'problemname', 'benchmode', 'grid']

benchmarkpy = 'benchmark_plot.py'

root = sys.argv[1]
dirs = [x[0] for x in os.walk(root)]

root_depth = len(root.split('/'))-1

plot_dirs = [i for i in dirs if len(i.split('/')) - len(subfolders) == root_depth]

for i in plot_dirs:
    print(i.split('/')[root_depth:])

    arch, compiler, backend, problem, benchmode, _ = i.split('/')[root_depth:]
    files = [f for f in os.listdir(i) if f.endswith('json')]

    if not files:
        print("No data files?")
        sys.exit(0)

    # fix file expected name
    for f in files:
        old_name = i + '/' + f
        new_name = i + '/' + f.replace('_at[aggressive]','_at[True]')
        os.rename(old_name, new_name)

    if arch not in machines:
        print("Don't own any information for %s architecture" % arch)
        sys.exit(0)

    machine = machines[arch]

    grid = re.search(r"shape\[([A-Za-z0-9_,]+)\]", files[0]).group(1).split(',')
    space_orders = sorted(set([int(re.search(r"so\[(\w+)\]", j).group(1)) for j in files]))
    tn = re.search(r"tn\[(\w+)\]", files[0]).group(1)

    args = ['python3', benchmarkpy, 'plot', '-P', problem, '-bm', benchmode]  # plot cmd
    args += ['-d'] + grid  # grid shape

    for j in space_orders:
        args += ['-so', str(j)]  # space orders tested

    args += ['-to', '2', '--tn', tn]  # time-related simulation parameters
    args += ['-r', i]  # plot dir
    args += ['--autotune']
    args += ['--backend', backend]

    # mode with linpack-peak
    args += ['--arch', 'unknown', '--max-bw', str(machine['dram-stream-bw']),  # arch params
             '--flop-ceil', str(machine['machine-peak']), 'ideal',
             '--flop-ceil', str(machine['linpack-peak']), 'linpack']

    call(' '.join(args), shell=True)

    print('\n')
